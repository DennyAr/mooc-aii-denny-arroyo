FROM bitnami/pytorch:latest

USER root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libglib2.0-0 \
    && rm -rf /var/lib/apt/lists/*


USER 1001


ADD req.txt .
RUN pip install -r req.txt
WORKDIR /usr/src/predict
ADD 7_predict.py .
ADD dogs_cats_model.pth .

CMD ["python", "7_predict.py"]
